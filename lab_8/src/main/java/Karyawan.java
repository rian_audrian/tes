import java.util.ArrayList;

public class Karyawan {

    private String name;
    private int salary;
    private int timesPaid = 0;

    public Karyawan(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() { return this.name; }

    public int getSalary() { return this.salary; }

    public void setSalary(int amount) { this.salary = amount; }

    public ArrayList<Karyawan> getSubordinates() { return null; }

    public boolean removeSubordinate(Karyawan k) { return false; }

    public String getRank() { return this.getClass().getSimpleName(); }

    public int getTimesPaid() { return this.timesPaid; }

    public void setTimesPaid(int i) { this.timesPaid = i; }

    public boolean canBePromoted() { return false; }

    public int canMakeSubordinate(Karyawan k) { return 1; }

    public void makeSubordinate(Karyawan k) {}

}