public class ArrayTest {
	
	public static void main(String[] args) {
		//Initialize array
		int array[][] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
		draw2DArray(array);
		
		//Change array content test
		changeArrayContent(array, 0, 0, 69);
		System.out.println();
		draw2DArray(array);
	}
	
	public static void draw2DArray(int[][] arr) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(arr[i][j] + " | ");
			}
		System.out.println();
		}
	}
	
	public static void changeArrayContent(int[][] arr, int xpos, int ypos, int value) {
		arr[xpos][ypos] = value;
	}
}