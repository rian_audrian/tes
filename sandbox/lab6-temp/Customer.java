package customer;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;
public class Customer {
	
	private String name;
	private int age;
	private boolean isFemale;
	
	public Customer(String name, boolean isFemale, int age) {
		this.name = name;
		this.age = age;
		this.isFemale = isFemale;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public boolean isFemale() {
		return this.isFemale;
	}
	
	public void findMovie(Theater theater, String title) {
		Movie[] availableMovies = theater.getAvailableMovies();
		
		boolean isAvailable = false;
		//Check movie availability
		for (Movie movie : availableMovies) {
			if (movie.getTitle().equals(title)) {
				movie.printInfo();
			isAvailable = true;
			break;
			}
		}
		
		//Print error message
		if (!isAvailable) {
			System.out.println(
				"Film " + title +
				" yang dicari " + this.getName() +
				" tidak ada di bioskop " + theater.getName());
		}
	}
	
	public Ticket orderTicket(Theater theater, String title, String day, String type) {
		Ticket query = null;
		
		//Create dummy ticket
		boolean dummy3D = false;
		if (type.equals("3 Dimensi")) dummy3D = true;
		Ticket dummy = new Ticket(theater.getMovie(title), day, dummy3D); 
		for (Ticket ticket : theater.getAvailableTickets()) {
			if (ticket.equals(dummy)) {
				query = ticket;
			}
		}
		
		if (query == null) {
			//Error: Ticket unavailable
			System.out.println("Tiket untuk film " + title + " jenis " + type + 
							   " tidak tersedia di " + theater.getName());
		} else {
			//Verify age
			if (this.getAge() < query.getMovie().getMinimumAge()) {
				//Error: Not suitable for minors
				System.out.println(this.getName() + " masih belum cukup umur untuk menonton " +
								   title + " dengan rating " + query.getMovie().getRating());
			} else {
				//Print success message
				System.out.println(this.getName() + " telah membeli tiket " +
								   title + " di " + theater.getName() +
								   " pada hari " + day + " seharga " + query.getPrice());
				theater.setBalance(theater.getBalance() + query.getPrice());
				return query;
			}
		}
		return null;
	}
}
