package character;

import java.util.ArrayList;

public class Player {

    protected String name;
    protected int health;
    protected boolean isBurned;

    ArrayList<Player> diet = new ArrayList<Player>();

    public Player(String name, int health) {
        this.name = name;
        this.health = health;
    }

    public String getName() { return this.name; }

    public int getHealth() { return this.health; }

    public void setHealth(int health) {
        this.health = health;
        if (this.health < 0) this.health = 0;
    }

    public boolean isDead() { return (this.health <= 0); }

    public boolean isBurned() { return this.isBurned; }

    public String getType() { return this.getClass().getSimpleName(); }

    public String status() {
        String info = this.getType() + " " + this.getName();
        
        String health = "HP: " + this.getHealth();

        String status = "Masih hidup";
        if (this.isDead()) status = "Sudah meninggal dunia dengan damai";
        
        String diet = this.diet();
        if (!this.diet.isEmpty()) diet = "Memakan " + this.diet();

        return String.format("%s\n%s\n%s\n%s", info, health, status, diet);
    }

    public String diet() {
        String output = new String();

        if (this.diet.size() == 0) {
            output = "Belum memakan siapa siapa";
        } else {
            for (int i = 0; i < this.diet.size(); i++) {
                output += this.diet.get(i).getType() + " " + this.diet.get(i).getName();
            }
        }
        return output;
    }

    public String attack(Player target) {
        if (!this.isDead()) {
            target.setHealth(target.getHealth() - 10);
            return String.format("Nyawa %s %d", target.getName(), target.getHealth());
        } else {
            return String.format("%s tidak bisa menyerang %s", this.getName(), target.getName());
        }
    }

    public boolean canEat(Player target) {
        return target.isDead();
    }

    public boolean eat(Player target) {
        if (this.canEat(target)) {
            this.diet.add(target);
            this.setHealth(this.getHealth() + 15);
            return true;
        } else {
            return false;
        }
    }

    public String roar() {
        return String.format("%s tidak bisa berteriak", this.getName());
    }

    public String burn(Player target) {
        return String.format("%s tidak bisa membakar %s", this.getName(), target.getName());
    }
}

//  write Player Class here
