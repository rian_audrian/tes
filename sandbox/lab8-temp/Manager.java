import java.util.Arrays;

public class Manager extends Karyawan {

    private Karyawan[] subordinates = new Karyawan[10];
    private int subordinatesCount = 0;

    public Manager(String name, int salary) {
        super(name, salary);
    }

    public int getSubordinatesCount() { return this.subordinatesCount; }

    public Karyawan[] getSubordinates() { return this.subordinates; }

    public boolean canMakeSubordinate(Karyawan k) {
        return (k.getRank().equals("Staff") || k.getRank().equals("Intern"));
    }

    public void makeSubordinate(Karyawan k) {
        int order = this.subordinatesCount;
        this.subordinates[order] = k;
        this.subordinatesCount++;
    }
}