public class Staff extends Manager {

    public static final int salaryLimit = 18000;

    public Staff(String name, int salary) {
        super(name, salary);
    }

    public boolean canMakeSubordinate(Karyawan k) {
        return (k.getRank().equals("Intern"));
    }

    public boolean canBePromoted() {
        return (this.getTimesPaid() >= 6);
    }
}