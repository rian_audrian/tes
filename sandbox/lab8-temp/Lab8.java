import java.util.Scanner;

public class Lab8 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Korporasi k = new Korporasi();
        boolean active = True;

        while (active) {
            String[] input = s.nextLine().split(" ");
            
            String command = input[0];
            
            if (command.equalsIgnoreCase("TAMBAH_KARYAWAN")) {
                try {
                    String nama = input[1];
                    String pangkat = input[2];
                    int gajiAwal = input[3];
                    k.add(nama, pangkat, gajiAwal);
                } catch (Exception e) {
                    System.out.println("Format: TAMBAH_KARYAWAN [nama] [pangkat] [gaji awal]");
                }
            } else if (command.equalsIgnoreCase("STATUS")) {
                try {
                    String nama = input[1];
                    k.find(nama);
                } catch (Exception e) {
                    System.out.println("Format: STATUS [nama]");
                }
            } else if (command.equalsIgnoreCase("TAMBAH_BAWAHAN")) {
                try {
                    String namaBos = input[i];
                    String namaBawahan = input[i];
                    k.tambahBawahan(namaBos, namaBawahan);
                } catch (Exception e) {
                    System.out.println("Format: TAMBAH_BAWAHAN [nama atasan] [nama bawahan]");
                }
            } else if (command.equalsIgnoreCase("GAJIAN")) {
                try {
                    k.gajian();
                } catch (Exception e) {
                    System.out.println("Format: GAJIAN");
                }
            } else {
                System.out.println("Perintah invalid");
            }
            
        }

        s.close();
    }
}