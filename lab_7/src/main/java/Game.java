import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (int i = 0; i < player.size(); i++) {
            Player temp = player.get(i);
            if (temp.getName().equals(name)) {
                return temp;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara) != null) {
            return String.format("Sudah ada karakter bernama %s", chara);
        } else {
            if (tipe.equals("Human")) {
                player.add(new Human(chara, hp));
            } else if (tipe.equals("Monster")) {
                player.add(new Monster(chara, hp));
            } else if (tipe.equals("Magician")) {
                player.add(new Magician(chara, hp));
            } else {
                return "Hanya bisa membuat human, magician atau monster";
            }
            return String.format("%s ditambah ke game", chara);
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        return "";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if (find(chara) != null) {
            player.remove(find(chara));
            return String.format("%s dihapus dari game", chara);
        } else {
            return String.format("Tidak ada %s", chara);
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        if (find(chara) != null) return find(chara).status();
        else return String.format("Tidak ada %s", chara);
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String output = new String();
        for (int i = 0; i < player.size(); i++) {
            output += player.get(i).status();
            if (i != player.size() -1 ) output += "\n";       
        }
        return output;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        if (find(chara) != null) {
            return find(chara).diet();
        } else {
            return String.format("Tidak ada %s", chara);
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String output = new String();
        for (int i = 0; i < player.size(); i++) {
            output += player.get(i).diet() + "\n";
        }
        return output;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);

        if (me != null && enemy != null) {
            return me.attack(enemy);
        } else {
            return String.format("Tidak ada %s atau %s", meName, enemyName);
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);

        if (me != null && enemy != null) {
            return me.burn(enemy);
        } else {
            return String.format("Tidak ada %s atau %s", meName, enemyName);
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);

        if (me != null && enemy != null) {
            if (me.eat(enemy) == true) {
                remove(enemyName);
                return String.format("%s memakan %s\nNyawa %s kini %d", meName, enemyName, meName, me.getHealth());
            } else {
                return String.format("%s tidak bisa memakan %s", meName, enemyName);
            }
        } else {
            return String.format("Tidak ada %s atau %s", meName, enemyName);
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player me = find(meName);

        if (me != null) {
            return me.roar();
        } else {
            return "Tidak ada " + meName;
        }
    }
}