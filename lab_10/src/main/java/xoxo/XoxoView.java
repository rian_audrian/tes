package xoxo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Audrian AP
 */
public class XoxoView {

    /**
     * Application main window
     */
    private JFrame mainFrame;
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Button that writes all previous encryptions/decryptions to a pre-specified file.
     */
    private JButton writeToFileButton;

    /**
     * Panel that contains key field and seed field
     */
    private JPanel topPanel;

    /**
     * Panel that contains text and log field
     */
    private JPanel centerPanel;

    /**
     * Panel that contains buttons to encrypt and decrypt
     */
    private JPanel bottomPanel;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
        mainFrame = new JFrame("CryptoXoxo");
        mainFrame.setSize(400, 300);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;

        initTopPanel();

        initCenterPanel();

        initBottomPanel();

        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private void initTopPanel() {
        topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());
        topPanel.setBorder(BorderFactory.createEtchedBorder());

        topPanel.add(new JLabel("Key: "));
        keyField = new JTextField("");
        keyField.setPreferredSize(new Dimension(100, 20));
        topPanel.add(keyField);

        topPanel.add(new JLabel("Seed: "));
        seedField = new JTextField("");
        seedField.setPreferredSize(new Dimension(100, 20));
        topPanel.add(seedField);

        mainFrame.add(topPanel, BorderLayout.NORTH);
    }

    private void initCenterPanel() {
        centerPanel = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel.setBorder(BorderFactory.createEtchedBorder());

        messageField = new JTextField("Enter message to be decrypted/encrypted here");
        messageField.setMaximumSize(new Dimension(640, 20));
        messageField.setPreferredSize(new Dimension(640, 20));
        centerPanel.add(messageField);

        logField = new JTextArea();
        logField.setEditable(false);
        logField.setCaretPosition(logField.getDocument().getLength());

        JScrollPane logScroll = new JScrollPane(logField);
        logScroll.setMinimumSize(new Dimension(640, 100));
        logScroll.setPreferredSize(new Dimension(640, 100));

        centerPanel.add(logScroll);

        mainFrame.add(centerPanel, BorderLayout.CENTER);
    }

    private void initBottomPanel() {
        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout());
        bottomPanel.setBorder(BorderFactory.createEtchedBorder());

        encryptButton = new JButton("Encrypt!");
        bottomPanel.add(encryptButton);

        decryptButton = new JButton("Decrypt!");
        bottomPanel.add(decryptButton);

        JButton clearLogButton = new JButton("Clear Log");
        clearLogButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                logField.setText("");
            }
        });
        bottomPanel.add(clearLogButton);

        writeToFileButton = new JButton("Write to file");
        bottomPanel.add(writeToFileButton);

        mainFrame.add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public void setWriteToFileFunction(ActionListener listener) { writeToFileButton.addActionListener(listener); }
}