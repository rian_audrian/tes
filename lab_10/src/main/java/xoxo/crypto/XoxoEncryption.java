package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException{
        this.kissKey = new KissKey(kissKeyString);
        if (kissKeyString.length() > 28) {
            throw new KeyTooLongException("Kiss key string exceeds 28 characters");
        }
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if seed is not between 0-36 (inclusive) <complete this>
     */
    public XoxoMessage encrypt(String message, int seed) {
        if (seed <= 0 || seed >= 36) {
            throw new RangeExceededException("Seed must be between 0 and 36 (inclusive)");
        }
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if message exceeds 10Kbits in size <complete this>
     * @throws InvalidCharacterException if a character is not on the valid list, i.e A-Z, a-z, 0-9 <complete this>
     */
    private String encryptMessage(String message) {
        //String comparator = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@";

        if (message.length() > 1250) {
            throw new SizeTooBigException("Message exceeds 10Kbits");
        }


        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            char m = message.charAt(i);
            if (!String.valueOf(m).matches("^[-a-zA-Z@._]")) {
                throw new InvalidCharacterException("Message contains invalid character");
            }
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}

