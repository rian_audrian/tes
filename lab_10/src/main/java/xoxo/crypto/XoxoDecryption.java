package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.SizeTooBigException;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        if (hugKeyString.length() > 28) {
            throw new KeyTooLongException("Key length exceeds 28 characters");
        }
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        String decryptedMessage = "";
        if (encryptedMessage.length() > 1250) {
            throw new SizeTooBigException("Message length cannot exceed 10Kbits");
        }
        for (int i = 0; i < encryptedMessage.length(); i++) {
            char temp = encryptedMessage.charAt(i);
            int key = (hugKeyString.charAt(i % hugKeyString.length()) ^ seed) - 'a';
            char result = (char) (key ^ temp);
            decryptedMessage += result;
        }

        return decryptedMessage;
    }
}