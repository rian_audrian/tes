package lab9.user;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import lab9.event.*;

public class User {

    private String name;
    private ArrayList<Event> events;

    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    public String getName() { return this.name; }

    boolean isOverlapping(Event a, Event b) {
        LocalDateTime startA = a.getStartDate();
        LocalDateTime startB = b.getStartDate();
        LocalDateTime endA = a.getEndDate();
        LocalDateTime endB = b.getEndDate();

        return startA.isBefore(endB) && startB.isBefore(endA);
    }

    public boolean addEvent(Event newEvent) {
        for (Event e : this.events) {
            if (isOverlapping(newEvent, e)) {
                return false;
            }
        }

        try {
            this.events.add(newEvent);
            return true;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<Event> getEvents() {
        ArrayList<Event> sortedList = new ArrayList<>(this.events);
        Collections.sort(sortedList, (e1, e2) -> e1.getStartDate().compareTo(e2.getStartDate()));
        return sortedList;
    }

    public BigInteger getTotalCost() {
        BigInteger output = BigInteger.valueOf(0);

        for (Event e : this.events) {
            output =  output.add(e.getCost());
        }

        return output;
    }
}
