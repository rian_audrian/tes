import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Muhammad Audrian, NPM 1706043790, Kelas E, GitLab Account: rian_audrian
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine;
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine;
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.next());
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.next());
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = Integer.parseInt(input.next());
		System.out.print("Berat Tubuh (kg)       : ");
		int berat = Integer.parseInt(input.next());
		System.out.print("Jumlah Anggota Keluarga: ");
		int jumlahAnggota = Integer.parseInt(input.next());
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine;
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine;
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = Integer.parseInt(input.next());
		
		System.out.print(panjang, lebar, tinggi, berat, jumlahAnggota);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		float rasio = (berat) / (panjang*(lebar/100)*(tinggi/100));

		/* for (.....) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + ... + " dari " + jumlahCetakan + " untuk: ");
			String penerima = .....; // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (.....) catatan = .....;
			else catatan = .....;

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "";
			.....;
		} **/


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = "";

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		... anggaran = (...) (...);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		... tahunLahir = .....; // lihat hint jika bingung
		... umur = (...) (...);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)





		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "";
		.....;

		input.close(); 
	}
}