import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Muhammad Audrian, NPM 1706043790, Kelas E, GitLab Account: rian_audrian
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.next());
		if (panjang < 0 || panjang > 250){
			System.out.println("0 <= panjang <= 250cm, harus bilangan bulat");
			System.exit(1);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.next());
		if (lebar < 0 || lebar > 250){
			System.out.println("0 <= lebar <= 250cm, harus bilangan bulat");
			System.exit(1);
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = Integer.parseInt(input.next());
		if (tinggi < 0 || tinggi > 250){
			System.out.println("0 <= tinggi <= 250cm, harus bilangan bulat");
			System.exit(1);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.next());
		if (berat < 0 || berat > 150){
			System.out.println("0 <= berat <= 150kg, harus bilangan riil");
			System.exit(1);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		int jumlahAnggota = Integer.parseInt(input.next());
		if (jumlahAnggota < 0 || jumlahAnggota > 20){
			System.out.println("0 <= jumlahAnggota <= 20, harus bilangan bulat");
			System.exit(1);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.next();
		input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = input.nextInt();
		input.nextLine();
		
		// Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// Hitung rasio berat per volume (rumus lihat soal)
		
		float temp = (berat / ( ((float)panjang/100) * ((float)tinggi/100) * ((float)lebar/100)));
		int rasio = (int) temp;

		 for (int i = 0; i < jumlahCetakan; i++) {
			 System.out.println("\nPencetakan " + Integer.toString(i+1) + " dari " + Integer.toString(jumlahCetakan) + " untuk:");
			 String penerima = input.nextLine().toUpperCase();
			 			 
			 System.out.println("DATA SIAP DICETAK UNTUK " + penerima);
			 System.out.println("---------");
			 System.out.println(nama + " - " + alamat);
			 System.out.println("Lahir pada " + tanggalLahir);
			 System.out.println("Rasio berat per volume: " + rasio + " kg/m^3");
			 if (catatan.isEmpty()) System.out.println("Tidak ada catatan tambahan");
			 else System.out.println("Catatan: " + catatan);
		}

		// Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		// rumus: ((panjang*tinggi*lebar) + jumlah ascii tiap huruf nama) mod 100
		
		int nameValue = 0;
		
		for (int i = 0; i < nama.length(); i++) {
			int value = nama.charAt(i);
			nameValue = nameValue + value;
		}
		
		float temp2 = ((panjang * tinggi * lebar) + nameValue) % 100;
		int temp3 = (int) temp2;
		
		// Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + Integer.toString(nameValue) + Integer.toString(temp3);
		
		// Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000 * 365 * jumlahAnggota;

		// Hitung umur dari tanggalLahir (rumus lihat soal)
		int tahunLahir = Integer.parseInt(tanggalLahir.split("-")[2]);
		int umur = 2018 - tahunLahir;

		// Lakukan proses menentukan apartemen (kriteria lihat soal)
		
		String apartemen = "";
		String kabupaten = "";	
				
		if (umur < 19){
			apartemen = apartemen + "PPMT";
			kabupaten = kabupaten + "Rotunda";
		}
		else if (umur >= 19 && umur <= 1018){
			if (anggaran <= 100000000){
				apartemen = apartemen + "Teksas";
				kabupaten = kabupaten + "Sastra";
			}
			else {
				apartemen = apartemen + "Mares";
				kabupaten = kabupaten + "Margonda";
			}
		}

		// Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		System.out.println("\nREKOMENDASI APARTEMEN");
		System.out.println("---------------------");
		System.out.println("Mengetahui: Identitas keluarga: " + nama + " " + nomorKeluarga);
		System.out.println("Menimbang: Anggaran makanan tahunan: Rp " + anggaran);
		System.out.println("           Umur kepala keluarga: " + umur);
		System.out.println("Memutuskan: Keluarga " + nama + " akan ditempatkan di: ");
		System.out.println("            " + apartemen + " kabupaten " + kabupaten);
		
		input.close();
	}
}